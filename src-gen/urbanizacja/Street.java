package urbanizacja;

class Street  implements NumbersHaver
 {
	// Konstruktor
	public Street()
	{
		// TODO: Constructor
	}
	// Atrybuty:
	private   Set<int>  Numbers ;
	private   String Name ;
	
	// Getters & Setters:
	public Set<int> 	getNumbers() { return Numbers; }
	 public void	setNumbers(Set<int>  w) { Numbers = w; }
	public String	getName() { return Name; }
	 public void	setName(String w) { Name = w; }
	
	// Operacje:	
	public  boolean isNumber() {
			return !Numbers.empty();
		
	}	
	public   void  crossing() {
			Name = "skrzyżowanie";
		
	}	
	
	
};
